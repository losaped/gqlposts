package main

import (
	"os"
	"strconv"

	"bitbucket.org/losaped/gqlposts/cachedstore"
	"bitbucket.org/losaped/gqlposts/postgres"
	"bitbucket.org/losaped/gqlposts/serializer"

	"bitbucket.org/losaped/gqlposts/api"
	"bitbucket.org/losaped/gqlposts/redis"
	log "github.com/sirupsen/logrus"
)

func main() {

	pgaddr := os.Getenv("PG_ADDR")
	pgbase := os.Getenv("PG_BASE")
	pgUser := os.Getenv("PG_USER")
	pgPass := os.Getenv("PG_PASS")
	redisAddr := os.Getenv("REDIS_ADDR")
	redisPass := os.Getenv("REDIS_PASS")
	redisBase := os.Getenv("REDIS_BASE")
	apiAddr := os.Getenv("API_ADDR")

	redisBaseInt, err := strconv.Atoi(redisBase)
	if err != nil {
		log.Fatal("unable read redis base")
	}

	postStore := postgres.CreatePostService(pgaddr, pgbase, pgUser, pgPass)

	if err := postStore.InitBase(); err != nil {
		log.Fatal(err)
	}

	postCache := redis.CreatePostCache(redisAddr, redisPass, redisBaseInt, 0)
	postService := cachedstore.PostService{
		Cache:      postCache,
		Store:      postStore,
		Serializer: serializer.JsonSerializer{},
	}

	postViewsCounter := redis.CreatePostViewsCounter(redisAddr, redisPass, redisBaseInt)
	api.RunAPI(postService, postViewsCounter, apiAddr)
}
