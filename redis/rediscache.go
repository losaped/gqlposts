package redis

import (
	"strconv"
	"time"

	"github.com/go-redis/redis"
)

const postSet = "post"

type PostCache struct {
	cli *redis.Client
	ttl time.Duration
}

func CreatePostCache(addr, pass string, db int, ttl time.Duration) PostCache {
	return PostCache{
		cli: redis.NewClient(&redis.Options{
			Addr:     addr,
			Password: pass,
			DB:       db,
		}),
		ttl: ttl,
	}
}

func (pc PostCache) Get(id uint64) ([]byte, error) {
	res, err := pc.cli.HGet(postSet, combineKey(id)).Result()
	return []byte(res), err
}

func (pc PostCache) Set(id uint64, data []byte) error {
	return pc.cli.HSet(postSet, combineKey(id), data).Err()
}

func (pc PostCache) Delete(id uint64) {
	pc.cli.HDel(postSet, combineKey(id))
}

func combineKey(id uint64) string {
	return strconv.FormatInt(int64(id), 10)
}
