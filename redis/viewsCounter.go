package redis

import (
	"strconv"

	"github.com/go-redis/redis"
	log "github.com/sirupsen/logrus"
)

func CreatePostViewsCounter(addr, pass string, db int) PostViewsCounter {
	return PostViewsCounter{
		cli: redis.NewClient(&redis.Options{
			Addr:     addr,
			Password: pass,
			DB:       db,
		}),
	}
}

type PostViewsCounter struct {
	cli *redis.Client
}

func (vc PostViewsCounter) Increase(key uint64) error {
	if err := vc.cli.Incr(strconv.Itoa(int(key))).Err(); err != nil {
		log.WithFields(log.Fields{"error": err, "key": key}).Error("on incrementing post counter")
		return err
	}

	return nil
}

func (vc PostViewsCounter) Count(key uint64) (int32, error) {
	cmd := vc.cli.Get(strconv.Itoa(int(key)))
	if err := cmd.Err(); err != nil {
		log.WithFields(log.Fields{"error": err, "key": key}).Error("on get post views")
		return 0, err
	}

	val, err := cmd.Int()
	return int32(val), err
}
