#! /bin/bash
export API_ADDR=127.0.0.1:7800
export REDIS_BASE=0
export REDIS_ADDR=127.0.0.1:6379
export PG_ADDR=127.0.0.1:5432
export PG_BASE=blog
export PG_USER=postgres
export PG_PASS=postgres

cd cmd
go build && ./cmd