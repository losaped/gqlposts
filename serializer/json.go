package serializer

import (
	"encoding/json"

	"bitbucket.org/losaped/gqlposts"
)

type JsonSerializer struct{}

func (s JsonSerializer) Marshal(p gqlposts.Post) ([]byte, error) {
	return json.Marshal(p)
}

func (s JsonSerializer) Unmarshal(raw []byte, p *gqlposts.Post) error {
	return json.Unmarshal(raw, p)
}
