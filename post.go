package gqlposts

// Post base Post struct
type Post struct {
	Id          uint64
	Title       string // Название
	Description string // Краткое описание
	Preview     string // Превью
	Content     string // Контент
	URI         string // URI
}

type Serializer interface {
	Marshal(p Post) ([]byte, error)
	Unmarshal(raw []byte, v *Post) error
}

type PostService interface {
	Create(*Post) error
	Delete(uint64) error
	Save(*Post) error
	List(limit, offset int32) ([]Post, error)
	Post(uint64) (*Post, error)
}

type ViewsCounter interface {
	Increase(uint64) error
	Count(uint64) (int32, error)
}

type PostCache interface {
	Get(uint64) ([]byte, error)
	Set(uint64, []byte) error
	Delete(uint64)
}
