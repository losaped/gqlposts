package api

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"bitbucket.org/losaped/gqlposts"
	"bitbucket.org/losaped/gqlposts/cachedstore"
	"bitbucket.org/losaped/gqlposts/serializer"
)

var maxItemsCount = 10

// implement ViewsCounter
type testViewsCounter struct {
	store []int
}

func (vc *testViewsCounter) Increase(id uint64) error {
	vc.store[int(id)] = vc.store[int(id)] + 1
	return nil
}

func (vc *testViewsCounter) Count(id uint64) (int32, error) {
	return int32(vc.store[int(id)]), nil
}

func createViewsCounter() *testViewsCounter {
	return &testViewsCounter{
		store: make([]int, maxItemsCount),
	}
}

// implement PostService
type testPostService struct {
	store []gqlposts.Post
}

func (ps *testPostService) Create(post *gqlposts.Post) error {
	lastIndex := len(ps.store) - 1
	if lastIndex >= 0 {
		maxID := ps.store[lastIndex].Id
		post.Id = maxID + 1
	} else {
		post.Id = 1
	}

	ps.store = append(ps.store, *post)
	return nil
}

func (ps *testPostService) Delete(id uint64) error {
	idx := -1
	for i, post := range ps.store {
		if post.Id == id {
			idx = i
			break
		}
	}

	if idx < 0 {
		return nil
	}

	ps.store = append(ps.store[:idx], ps.store[idx+1:]...)
	return nil
}

func (ps *testPostService) Save(post *gqlposts.Post) error {
	for i := 0; i < len(ps.store); i++ {
		if post.Id == ps.store[i].Id {
			ps.store[i] = *post
			return nil
		}
	}

	return nil
}

func (ps *testPostService) List(limit, offset int32) ([]gqlposts.Post, error) {
	if int(offset) >= len(ps.store) {
		return nil, nil
	}

	tailLen := len(ps.store) - int(offset)
	if tailLen < int(limit) {
		limit = int32(tailLen)
	}

	return ps.store[offset : offset+limit], nil
}

func (ps *testPostService) Post(id uint64) (*gqlposts.Post, error) {
	for i := 0; i < len(ps.store); i++ {
		if ps.store[i].Id == id {
			return &ps.store[i], nil
		}
	}

	return nil, nil
}

func createPostService() *testPostService {
	return &testPostService{
		store: []gqlposts.Post{},
	}
}

// implements PostCache
type testPostCache struct {
	store map[uint64][]byte
}

func (pc *testPostCache) Get(id uint64) ([]byte, error) {
	return pc.store[id], nil
}

func (pc *testPostCache) Set(id uint64, data []byte) error {
	pc.store[id] = data
	return nil
}

func (pc *testPostCache) Delete(id uint64) {
	delete(pc.store, id)
}

func createPostCache() *testPostCache {
	return &testPostCache{
		store: make(map[uint64][]byte, maxItemsCount),
	}
}

type apiTest struct {
	step     string
	query    []byte
	expected string
}

func TestAPI(t *testing.T) {
	postService := cachedstore.PostService{
		Cache:      createPostCache(),
		Store:      createPostService(),
		Serializer: serializer.JsonSerializer{},
	}

	viewsCounter := createViewsCounter()

	go RunAPI(postService, viewsCounter, "127.0.0.1:7800")

	time.Sleep(1 * time.Second)

	tests := []apiTest{
		apiTest{
			step:     "create 1 post",
			query:    []byte(`{"query":"mutation CreateFirstPost($p: PostInput!) {\n  createPost(post: $p){\n    id\n    title\n  }\n}","variables":{"p":{"title":"first post","description":"description for first post","preview":"preview for first post","content":"content for first post","uri":"/post/1"}},"operationName":"CreateFirstPost"}`),
			expected: `{"data":{"createPost":{"id":"1","title":"first post"}}}`,
		},
		apiTest{
			step:     "create 2 post",
			query:    []byte(`{"query":"mutation CreateFirstPost($p: PostInput!) {\n  createPost(post: $p){\n    id\n    title\n  }\n}","variables":{"p":{"title":"2 post","description":"description for 2 post","preview":"preview for 2 post","content":"content for 2 post","uri":"/post/2"}},"operationName":"CreateFirstPost"}`),
			expected: `{"data":{"createPost":{"id":"2","title":"2 post"}}}`,
		},
		apiTest{
			step:     "create 3 post",
			query:    []byte(`{"query":"mutation CreateFirstPost($p: PostInput!) {\n  createPost(post: $p){\n    id\n    title\n  }\n}","variables":{"p":{"title":"3 post","description":"description for 3 post","preview":"preview for 3 post","content":"content for 3 post","uri":"/post/3"}},"operationName":"CreateFirstPost"}`),
			expected: `{"data":{"createPost":{"id":"3","title":"3 post"}}}`,
		},
		apiTest{
			step:     "create 4 post",
			query:    []byte(`{"query":"mutation CreateFirstPost($p: PostInput!) {\n  createPost(post: $p){\n    id\n    title\n  }\n}","variables":{"p":{"title":"4 post","description":"description for 4 post","preview":"preview for 4 post","content":"content for 4 post","uri":"/post/4"}},"operationName":"CreateFirstPost"}`),
			expected: `{"data":{"createPost":{"id":"4","title":"4 post"}}}`,
		},
		apiTest{
			step:     "create 5 post",
			query:    []byte(`{"query":"mutation CreateFirstPost($p: PostInput!) {\n  createPost(post: $p){\n    id\n    title\n  }\n}","variables":{"p":{"title":"5 post","description":"description for 5 post","preview":"preview for 5 post","content":"content for 5 post","uri":"/post/5"}},"operationName":"CreateFirstPost"}`),
			expected: `{"data":{"createPost":{"id":"5","title":"5 post"}}}`,
		},
		apiTest{
			step:     "get post with id 3",
			query:    []byte(`{"query":"query GetPost($id: ID!) {\n  getPost(id: $id){\n    preview\n    content\n    viewsCount\n  }\n}","variables":{"id":3},"operationName":"GetPost"}`),
			expected: `{"data":{"getPost":{"preview":"preview for 3 post","content":"content for 3 post","viewsCount":1}}}`,
		},
		apiTest{
			step:     "get post with id 3 increased views count",
			query:    []byte(`{"query":"query GetPost($id: ID!) {\n  getPost(id: $id){\n    preview\n    content\n    viewsCount\n  }\n}","variables":{"id":3},"operationName":"GetPost"}`),
			expected: `{"data":{"getPost":{"preview":"preview for 3 post","content":"content for 3 post","viewsCount":2}}}`,
		},
		apiTest{
			step:     "get posts 2 2",
			query:    []byte(`{"query":"query PostList($l: Int!, $of: Int!) {\n  getPosts(limit: $l, offset: $of){\n   title\n    uri\n  }\n}","variables":{"l":2,"of":2},"operationName":"PostList"}`),
			expected: `{"data":{"getPosts":[{"title":"3 post","uri":"/post/3"},{"title":"4 post","uri":"/post/4"}]}}`,
		},
		apiTest{
			step:     "delete post 3",
			query:    []byte(`{"query":"mutation DeletePost($id: ID!) {\n  deletePost(id: $id)\n}","variables":{"id":3},"operationName":"DeletePost"}`),
			expected: `{"data":{"deletePost":true}}`,
		},
		apiTest{
			step:     "get posts 2 2 without id=3",
			query:    []byte(`{"query":"query PostList($l: Int!, $of: Int!) {\n  getPosts(limit: $l, offset: $of){\n   title\n    uri\n  }\n}","variables":{"l":2,"of":2},"operationName":"PostList"}`),
			expected: `{"data":{"getPosts":[{"title":"4 post","uri":"/post/4"},{"title":"5 post","uri":"/post/5"}]}}`,
		},
	}

	for _, test := range tests {
		oneRequest(t, test)
	}
}

func oneRequest(t *testing.T, test apiTest) {
	bbuf := bytes.NewBuffer([]byte(test.query))

	r, err := http.DefaultClient.Post("http://127.0.0.1:7800/graphql", "application/json", bbuf)
	if err != nil {
		t.Error(err)
	}

	resp, err := ioutil.ReadAll(r.Body)
	if err != nil {
		t.Error(err)
	}

	r.Body.Close()
	assert.Equalf(t, test.expected, string(resp), `on step "%s"`, test.step)
}
