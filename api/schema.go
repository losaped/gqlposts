package api

var rawSchema = `schema {
    query: Query
    mutation: Mutation
}

type Query {
    getPost(id: ID!): Post
    getPosts(limit: Int!, offset: Int!): [Post]
}

type Mutation {
    createPost(post: PostInput!): Post
    updatePost(post: PostInput!): Post
    deletePost(id: ID!): Boolean
}

type Post {
    id: ID
	title: String
	description: String
	preview: String
	content: String
	uri: String
	viewsCount: Int
}

input PostInput {
    title: String!
    description: String!
	preview: String!
	content: String!
	uri: String!
}
`
