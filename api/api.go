package api

import (
	log "github.com/sirupsen/logrus"

	"bitbucket.org/losaped/gqlposts"
	"github.com/gramework/gramework"
	"github.com/gramework/gramework/graphiql"
	graphql "github.com/graph-gophers/graphql-go"
	"github.com/graph-gophers/graphql-go/relay"
)

func RunAPI(postSvc gqlposts.PostService, viewsCounter gqlposts.ViewsCounter, port string) {
	postResolver := &PostResolver{
		postService:  postSvc,
		viewsCounter: viewsCounter,
	}

	schema := graphql.MustParseSchema(rawSchema, postResolver)

	api := gramework.New()
	api.GET("/graphql", gramework.NewGrameHandler(&relay.Handler{Schema: schema}))
	api.POST("/graphql", gramework.NewGrameHandler(&relay.Handler{Schema: schema}))
	api.GET("/ide", graphiql.Handler)
	log.Error(api.ListenAndServe(port))
}
