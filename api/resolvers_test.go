package api

import (
	"testing"

	"bitbucket.org/losaped/gqlposts/redis"
	"github.com/stretchr/testify/require"
)

func TestViewsCount(t *testing.T) {
	vc := redis.CreatePostViewsCounter("localhost:6379", "", 0)
	vc.Increase(1)

	cnt, err := vc.Count(1)
	require.NoError(t, err)
	require.Equal(t, int32(1), cnt)
}
