package api

import (
	"context"
	"strconv"

	"bitbucket.org/losaped/gqlposts"
	graphql "github.com/graph-gophers/graphql-go"
	log "github.com/sirupsen/logrus"
)

type PostResolver struct {
	postService  gqlposts.PostService
	viewsCounter gqlposts.ViewsCounter
}

func (pr PostResolver) GetPost(ctx context.Context, args struct{ ID int32 }) (*postFieldResolver, error) {
	post, err := pr.postService.Post(uint64(args.ID))
	if err != nil {
		return nil, err
	}

	if err := pr.viewsCounter.Increase(post.Id); err != nil {
		log.WithFields(log.Fields{"error": err}).Error("on increas views count")
	}

	return &postFieldResolver{post: post, viewsCounter: pr.viewsCounter}, nil
}

func (pr PostResolver) GetPosts(ctx context.Context, args struct{ Limit, Offset int32 }) (*[]*postFieldResolver, error) {
	posts, err := pr.postService.List(args.Limit, args.Offset)
	if err != nil {
		return nil, err
	}

	resolvers := make([]*postFieldResolver, len(posts))
	for i, _ := range posts {
		resolvers[i] = &postFieldResolver{
			post:         &posts[i],
			viewsCounter: pr.viewsCounter,
		}
	}

	return &resolvers, nil
}

func (pr PostResolver) CreatePost(ctx context.Context, args struct{ Post gqlposts.Post }) (*postFieldResolver, error) {
	if err := pr.postService.Create(&args.Post); err != nil {
		return nil, err
	}

	return &postFieldResolver{
		post:         &args.Post,
		viewsCounter: pr.viewsCounter,
	}, nil
}

func (pr PostResolver) UpdatePost(ctx context.Context, args struct{ Post gqlposts.Post }) (*postFieldResolver, error) {
	if err := pr.postService.Save(&args.Post); err != nil {
		return nil, err
	}

	return &postFieldResolver{
		post:         &args.Post,
		viewsCounter: pr.viewsCounter,
	}, nil
}

func (pr PostResolver) DeletePost(ctx context.Context, args struct{ ID int32 }) (*bool, error) {
	if err := pr.postService.Delete(uint64(args.ID)); err != nil {
		return boolPointer(false), err
	}

	return boolPointer(true), nil
}

type postFieldResolver struct {
	post         *gqlposts.Post
	viewsCounter gqlposts.ViewsCounter
}

func (pr postFieldResolver) ID() *graphql.ID {
	id := graphql.ID(strconv.FormatInt(int64(pr.post.Id), 10))
	return &id
}

func (pr postFieldResolver) Title() *string {
	return &pr.post.Title
}

func (pr postFieldResolver) Description() *string {
	return &pr.post.Description
}

func (pr postFieldResolver) Preview() *string {
	return &pr.post.Preview
}

func (pr postFieldResolver) Content() *string {
	return &pr.post.Content
}

func (pr postFieldResolver) URI() *string {
	return &pr.post.URI
}

func (pr postFieldResolver) ViewsCount() *int32 {
	count, err := pr.viewsCounter.Count(pr.post.Id)
	if err != nil {
		log.WithFields(log.Fields{"error": err}).Error("on getting views count")
	}

	return &count
}

func boolPointer(val bool) *bool {
	return &val
}
