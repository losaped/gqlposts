FROM golang:latest
ADD . /go/src/bitbucket.org/losapd/gqlposts
WORKDIR /go/src/bitbucket.org/losapd/gqlposts/cmd
RUN go get -d
RUN go build
CMD [ "./cmd" ]
EXPOSE  7800