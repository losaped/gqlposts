package postgres

import (
	"bitbucket.org/losaped/gqlposts"
	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
)

type PostService struct {
	db *pg.DB
}

func CreatePostService(addr, dbName, dbuser, dbpass string) PostService {

	opts := &pg.Options{
		Addr:     addr,
		User:     dbuser,
		Password: dbpass,
		Database: dbName,
	}

	db := pg.Connect(opts)
	return PostService{db: db}
}

func (ps PostService) Create(p *gqlposts.Post) error {
	return ps.db.Insert(p)
}

func (ps PostService) Delete(id uint64) error {
	p := &gqlposts.Post{
		Id: id,
	}

	return ps.db.Delete(p)
}

func (ps PostService) Save(p *gqlposts.Post) error {
	return ps.db.Update(p)
}

func (ps PostService) List(limit, offset int32) ([]gqlposts.Post, error) {
	posts := &[]gqlposts.Post{}
	_, err := ps.db.Query(posts, `SELECT * FROM posts ORDER BY id LIMIT ? OFFSET ?`, limit, offset)
	return *posts, err
}

func (ps PostService) Post(id uint64) (*gqlposts.Post, error) {
	post := &gqlposts.Post{
		Id: id,
	}

	err := ps.db.Select(post)
	return post, err
}

func (ps PostService) InitBase() error {
	return ps.db.CreateTable(&gqlposts.Post{}, &orm.CreateTableOptions{
		IfNotExists: true,
	})
}
