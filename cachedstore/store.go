package cachedstore

import (
	"bitbucket.org/losaped/gqlposts"
)

type PostService struct {
	Cache      gqlposts.PostCache
	Store      gqlposts.PostService
	Serializer gqlposts.Serializer
}

func (ps PostService) Create(p *gqlposts.Post) error {
	return ps.Store.Create(p)
}

func (ps PostService) Delete(id uint64) error {
	ps.Cache.Delete(id)
	return ps.Store.Delete(id)
}

func (ps PostService) Save(p *gqlposts.Post) error {
	ps.Cache.Delete(p.Id)
	return ps.Store.Save(p)
}

func (ps PostService) List(limit, offset int32) ([]gqlposts.Post, error) {
	return ps.Store.List(limit, offset)
}

func (ps PostService) Post(id uint64) (*gqlposts.Post, error) {
	p := &gqlposts.Post{}

	raw, err := ps.Cache.Get(id)
	if err == nil {
		if err = ps.Serializer.Unmarshal(raw, p); err == nil {
			return p, nil
		}
	}

	p, err = ps.Store.Post(id)
	if err != nil {
		return p, err
	}

	raw, err = ps.Serializer.Marshal(*p)
	if err == nil {
		ps.Cache.Set(id, raw)
	}

	return p, nil
}
